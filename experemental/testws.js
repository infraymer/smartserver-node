const WebSocket = require('ws');
const api = require('../api-contract');
const hashpass = '18138372FAD4B94533CD4881F03DC6C69296DD897234E0CEE83F727E2E6B1F63'

var socket = new WebSocket("ws://localhost:50100/client", ["vlad:" + hashpass])

var json = {
  action: "getDataSmarthome",
  data: {
    idHome: 5
  }
}
  // action: "subsystemsInfo",
  // idHome: 1,
  // data: {
  //   idFun: 2,
  //   value: 1
  // }
// }

socket.onopen = function() {
  console.log("Соединение установлено.");
  // socket.send("hello")
};

socket.onclose = function(event) {
  if (event.wasClean) {
    console.log('Соединение закрыто чисто');
  } else {
    console.log('Обрыв соединения'); // например, "убит" процесс сервера
  }
  console.log('Код: ' + event.code + ' причина: ' + event.reason);
};

socket.onmessage = function(event) {
  console.log("Получены данные " + event.data);
  // socket.send(JSON.stringify(json));
};

socket.onerror = function(error) {
  console.log("Ошибка " + error.message);
};

// socket.on('message', function incomipng(data){
//   console.log(data);
// });
