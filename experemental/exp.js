const map = new Map([
    [0, "kek0"],
    [1, "kek1"],
    [2, "kek2"]
]);

const arr = [
    {
        id: 0,
        name: "qwe"
    },
    {
        id: 3,
        name: "asd"
    }
];

for (let item of arr) {
    let kek = map.get(item);
    item.online = kek !== undefined;
}

console.log(JSON.stringify(arr));