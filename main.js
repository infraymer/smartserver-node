// Logging
let log4js = require('log4js');
let logger = log4js.getLogger();
logger.level = 'debug';

// Тут ты шаришь
const http = require('http');
const WebSocket = require('ws');
const API = require('./api-contract');
const sqlite3 = require('sqlite3').verbose();
const queryToDB = require('./query-to-db');
const connectionHolder = require('./connection-holder');

logger.info("START SERVER");

/**
 * Глобальные переменные
 */

// HTTP сервер на основе которого будут работать вебсокеты
const server = http.createServer();
// Вебсокет клиента
logger.info("WebSocket - CLIENT - CREATE");
const wssClient = new WebSocket.Server({noServer: true});
// Вебсокет умного дома
logger.info("WebSocket - SUBSYSTEM - CREATE");
const wssSubsystem = new WebSocket.Server({noServer: true});

logger.info("Database - CONNECTION");
let db = new sqlite3.Database('./db/smart_server.db', (err) => {
    if (err) {
        logger.error("Database - " + err);
    }
    logger.info("Database - CONNECTED");
});

let dbClose = function () {
    logger.info("Database - CLOSING");
    db.close((err) => {
        if (err) {
            logger.error("Database - " + err);
        }
        logger.info("Database - CLOSED");
    });
};


// Обработка входящего сообщения от клиента
const handlingClientMessages = function (idUser, data) {
    logger.debug("handlingClientMessages - " + idUser + " - " + data);
    // парсим json строку
    const message = JSON.parse(data);
    // Вызываем функцию обработки в зависимости от Action
    switch (message.action) {
        case API.callFunctionDevice:
            callFunctionDevice(idUser, message);
            break;
        case API.getDevices:
            getDevices(idUser, message.data);
            break;
        default:
            // redirectToHome(idUser, message);
            logger.error("handlingClientMessages - Нет такой функции");
    }
};

// Обработка входящего сообщения от умного дома
const handlingSubsystemMessages = function (idSubsystem, data) {
    logger.debug("handlingSubsystemMessages - " + idSubsystem + " - " + data);
    const message = JSON.parse(data);
    switch (message.action) {
        case API.deviceInfo:
            deviceInfo(idSubsystem, message);
            break;
        case API.devicesInfo:
            devicesInfo(idSubsystem, message);
            break;
        default:
            logger.error("handlingSubsystemMessages - Нет такой функции");
    }
};

/**
 * Вспомогательные функции
 */
// Проверяет данные на валидность пользователя
function authClient(name, pass, result) {
    logger.debug("authClient - name: " + name + ", password: " + pass);
    db.serialize(() => {
        let data = null;
        logger.debug("Database - SELECT");
        db.each(`SELECT idUser as id, login as name, password_hash as pass FROM Users WHERE name = '${name}'`,
            callback = (err, row) => {
                if (err) {
                    logger.error("Database - " + err);
                    result(false)
                }
                logger.debug("Database - " + JSON.stringify(row));
                // console.log(row.name + "\t" + row.pass);
                if (row.pass == pass) data = row;
            },
            complete = () => {
                result(data);
            }
        );
    });
}


// Проверяет данные на валидность умного дома
function authSubsystem(name, pass, result) {
    logger.debug("authSubsystem - name: " + name + ", password: " + pass);
    db.serialize(() => {
        let data = null;
        logger.debug("Database - SELECT");
        db.each(`SELECT idHome as id, name, password_hash as pass FROM smart_homes WHERE name = '${name}'`,
            callback = (err, row) => {
                if (err) {
                    logger.error("Database - " + err);
                    result(false)
                }
                logger.debug("Database - " + JSON.stringify(row));
                if (row.pass == pass) data = row;
            },
            complete = () => {
                result(data);
            }
        );
    });
}

/**
 * Функции обработки ACTION
 *
 */
function callFunctionDevice(idUser, message) {
    connectionHolder.sendToSubsystem(message.data.idHome, message);
}

function deviceInfo(idSubsystem, data) {
    data.data.idHome = idSubsystem;
    queryToDB.getIdUsersByHome(db, idSubsystem, (users) => {
        for (let user of users) connectionHolder.sendToUser(user.id, data);
    });
}

function devicesInfo(subsystem, message) {
    connectionHolder.sendToUser(message.data.idUser, message)
}

function subsystemsInfo(userID, subsystems) {
    let subsystemsInfo = {
        action: API.subsystemsInfo,
        data: subsystems
    };
    connectionHolder.sendToUser(userID, subsystemsInfo);
}

//Перенаправление данных на подсистему
function redirectToHome(idUser, data) {
    logger.debug("redirectToHome - idUser=" + idUser + ", data=" + JSON.stringify(data));
    data.data.idUser = idUser;
    connectionHolder.sendToSubsystem(data.data.idHome, data)
}

//Перенаправление данных пользователю
function redirectToUser(idHome, data) {
    logger.debug("redirectToUser - idHome=" + idHome + ", data=" + JSON.stringify(data));
    data.data.idHome = idHome;
    connectionHolder.sendToUser(data.data.idUser, data)
}

function getDevices(idUser, idHome) {
    logger.debug("getDevices - idUser=" + idUser + ", idHome=" + idHome);
    redirectToHome(idUser, {action: API.getDevices, data: {idHome: idHome, idUser: idUser}});
}

/**
 * Ответ и закрытие соекета
 */
function abortHandshake(socket, code, message, headers) {
    if (socket.writable) {
        message = message || http.STATUS_CODES[code];
        headers = Object.assign({
            'Connection': 'close',
            'Content-type': 'text/html',
            'Content-Length': Buffer.byteLength(message)
        }, headers);

        socket.write(
            `HTTP/1.1 ${code} ${http.STATUS_CODES[code]}\r\n` +
            Object.keys(headers).map(h => `${h}: ${headers[h]}`).join('\r\n') +
            '\r\n\r\n' +
            message
        );
    }
    socket.destroy();
}

/**
 * Обработка подключения КЛИЕНТА
 */
wssClient.on('connection', function connection(ws, idUser) {
    try {
        connectionHolder.addUserConnection(idUser, ws);
        logger.info("WebSocket - CLIENT - CONNECTED - user: " + idUser);
        queryToDB.getHomeByUser(db, idUser, (subsystems) => {
            try {
                if (subsystems == null) throw new Error("Subsystems is null");
                subsystems = connectionHolder.subsystemsIsOnline(subsystems);
                subsystemsInfo(idUser, subsystems);
                queryToDB.getHomeByUser(db, idUser, (homes) => {
                    for (home of homes) {
                        getDevices(idUser, home.id)
                    }
                });
            } catch (e) {
                logger.error(e);
            }
        });

        // Колбэк входящих сообщений
        ws.on('message', (message) => {
            try {
                logger.info("WebSocket - CLIENT - MESSAGE - id: " + idUser + ", message: " + message);
                handlingClientMessages(idUser, message);
            } catch (e) {
                logger.error("Получен не верный формат данных \n" + e);
                ws.send(JSON.stringify({error: "Неверный формат данных"}))
            }
        });
        // Колбэк закрытия соединения
        ws.on('close', () => {
            try {
                connectionHolder.removeUserConnection(idUser, ws);
                logger.info("WebSocket - CLIENT - DISCONNECTED - id: " + idUser);
            } catch (e) {
                logger.error(e);
            }

        });
    } catch (e) {
        ws.close();
        logger.error(e);
    }
});

/**
 * Обработка подключения УМНОГО ДОМА
 */
wssSubsystem.on('connection', function connection(ws, idSubsystem) {
    logger.info("WebSocket - SUBSYSTEM - CONNECTED - id: " + idSubsystem);
    connectionHolder.addSubsystemConnection(idSubsystem, ws);
    ws.on('message', (message) => {
        try {
            logger.info("WebSocket - SUBSYSTEM - MESSAGE - id: " + idSubsystem + ", message: " + message);
            handlingSubsystemMessages(idSubsystem, message);
        } catch (e) {
            logger.error(e);
        }

    });
    // Колбэк на закрытие соединения
    ws.on('close', (conn) => {
        connectionHolder.removeSubsystemConnection(idSubsystem);
        logger.info("WebSocket - SUBSYSTEM - CLOSED - id: " + idSubsystem);
    });
});

/**
 * Подключение
 */
function handleConnectionClient(request, socket, head) {
    logger.info("WebSocket - CLIENT - CONNECTION");
    try {
        let authError = new Error("Invalid auth data");
        // Читаем заголовок запроса в котором лежат данные авторизации, в данный момент формат такой: login:password_hash
        // const auth = request.headers['auth'];
        let auth = request.headers['auth'];
        if (auth === undefined)
            auth = request.headers['sec-websocket-protocol'];
        // Если данный заголовок отсутствует, то бросаем исключение, чтобы в блоке Catch закрылся websocket
        if (auth === undefined) throw authError;
        // Получаем пользователя и пароль из заголовка
        const [user, pass] = auth.split(':');
        // В дальнейшем нужно будет придумать кастомные коды закрытия сокета
        authClient(user, pass, (result) => {
            try {
                if (result == null) throw authError;
                let idUser = result.id;
                wssClient.handleUpgrade(request, socket, head, function done(ws) {
                    wssClient.emit('connection', ws, idUser);
                });
            } catch (e) {
                abortHandshake(socket, 401);
                logger.error(e);
            }
        });
    } catch (e) {
        // Если случилась какая-то ошибка, то закрываем соединение
        abortHandshake(socket, 400);
        logger.error(e);
    }
}

function handleConnectionSubsystem(request, socket, head) {
    logger.info("WebSocket - SUBSYSTEM - CONNECTION");
    try {
        let authError = new Error("Invalid auth data");
        // Читаем заголовок запроса в котором лежат данные авторизации, в данный момент формат такой: login:password_hash
        // const auth = request.headers['auth'];
        const auth = request.headers['sec-websocket-protocol'];
        // Если данный заголовок отсутствует, то бросаем исключение, чтобы в блоке Catch закрылся websocket
        if (auth === undefined) throw authError;
        // Получаем пользователя и пароль из заголовка
        const [user, pass] = auth.split(':');
        // В дальнейшем нужно будет придумать кастомные коды закрытия сокета
        authSubsystem(user, pass, (result) => {
            try {
                if (result == null) throw authError;
                let idSubsystem = result.id;
                wssSubsystem.handleUpgrade(request, socket, head, function done(ws) {
                    wssSubsystem.emit('connection', ws, idSubsystem);
                });
            } catch (e) {
                abortHandshake(socket, 401);
                logger.error(e);
            }
        });
    } catch (e) {
        // Если случилась какая-то ошибка, то закрываем соединение
        abortHandshake(socket, 400);
        logger.error(e);
    }
}


/**
 * Установка соединения с WebSocket
 */
server.on('upgrade', function upgrade(request, socket, head) {
    if (request.url === '/client') {
        handleConnectionClient(request, socket, head);
    } else if (request.url === '/subsystem') {
        handleConnectionSubsystem(request, socket, head)
    } else {
        abortHandshake(socket, 400);
    }
});

server.listen(50100);
