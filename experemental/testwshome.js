const WebSocket = require('ws');

var ws = new WebSocket("ws://localhost:50100/client", ["danay:qwe123"])

ws.onopen = function () {
    console.log("Соединение установлено.");
    // ws.send("hello")

    // setTimeout( function () {
    //     let m = {
    //         action: "callFunctionDevice",
    //         data: {
    //             idHome: 5,
    //             idDevice: 55,
    //             idFunction: 5,
    //             value: 1
    //         }
    //     }
    //     ws.send(JSON.stringify(m))
    // }, 2000)
};

ws.onclose = function (event) {
    if (event.wasClean) {
        console.log('Соединение закрыто чисто');
    } else {
        console.log('Обрыв соединения'); // например, "убит" процесс сервера
    }
    console.log('Код: ' + event.code + ' причина: ' + event.reason);
};

ws.onmessage = function (event) {
    console.log("Получены данные " + event.data);
    // ws.send("hello")
};

ws.onerror = function (error) {
    console.log("Ошибка " + error.message);
};
