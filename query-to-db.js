//функция получения всех умных домов которые доступны пользователю
module.exports.getHomeByUser = function (db, idUser, result) {
    db.serialize(() => {
        db.all(`select smart_homes.idHome as id, smart_homes.name as name
       from smart_homes inner join homes_users on id = homes_users.idHome
       left join Users on homes_users.idUser = Users.idUser
       where Users.idUser = ${idUser}`, (err, rows) => {
            if (err) {
                console.error(err.message);
                result(null)
            }
            // console.log(row.name + "\t" + row.pass);
            // console.log(rows);
            result(rows);
        });
    });
};

module.exports.getIdHomeByUser = function (db, idUser, result) {
    db.serialize(() => {
        db.all(`select idHome as id from homes_users where idUser = ${idUser}`,
            (err, rows) => {
                if (err) {
                    console.log(err.message);
                    result(null)
                }
                result(rows);
            });
    });
};

module.exports.getIdUsersByHome = function (db, idHome, result) {
    db.serialize(() => {
        db.all(`select idUser as id from homes_users where idHome = ${idHome}`,
            (err, rows) => {
                if (err) {
                    console.log(err.message);
                    result(null)
                }
                result(rows);
            });
    });
};
