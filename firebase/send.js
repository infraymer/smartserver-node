var admin = require("firebase-admin");

var serviceAccount = require("./smartserver-a4839-firebase-adminsdk-03w6d-4e50e0205b.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://smartserver-a4839.firebaseio.com"
});

var sendMsg = function(payload) {
  admin.messaging().send(payload)
  .then((response) => {
    // Response is a message ID string.
    console.log('Successfully sent message:', response);
  })
  .catch((error) => {
    console.log('Error sending message:', error);
  });
}

var payload = {
    data: {
      score: '850',
      time: '2:45'
    },
    token: "registrationToken"
};

sendMsg(payload);
