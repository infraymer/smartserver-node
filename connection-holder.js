let log4js = require('log4js');
let logger = log4js.getLogger();
logger.level = 'debug';

const clients = new Map();
const subsystems = new Map();

module.exports.sendToUser = function (id, data) {
    let webSockets = clients.get(id);
    if (webSockets !== undefined) {
        logger.info(`ConnectionHolder - SEND TO USER - user: ${id}, data: ${JSON.stringify(data)}`);
        for (const ws of webSockets) {
            logger.debug(`ws: ${ws.toString()}`);
            ws.send(JSON.stringify(data));
        }
    }
};

module.exports.sendToAllUserBySubsystem = function (id) {

};

module.exports.sendToSubsystem = function (id, data) {
    logger.info(`ConnectionHolder - SEND TO SUBSYSTEM - user: ${id}, data: ${JSON.stringify(data)}`);
    let ws = subsystems.get(id);
    if (ws !== undefined)
        ws.send(JSON.stringify(data));
};

module.exports.addUserConnection = function (id, ws) {
    if (clients.get(id) === undefined) {
        clients.set(id, [ws]);
    } else { // Иначе получаем массив по пользователю и кладем в него вебсокет
        clients.get(id).push(ws);
    }
};

module.exports.removeUserConnection = function (id, ws) {
    let index = clients.get(id).indexOf(ws);
    clients.get(id).splice(index, 1);
};

module.exports.removeAllUserConnections = function (id) {
    clients.delete(id);
};

module.exports.addSubsystemConnection = function (id, ws) {
    subsystems.set(id, ws);
};

module.exports.removeSubsystemConnection = function (id) {
    subsystems.delete(id);
};

module.exports.subsystemsIsOnline = function (list) {
    for (const item of list) {
        let subsystem = subsystems.get(item.id);
        item.online = subsystem !== undefined;
    }
    return list;
};