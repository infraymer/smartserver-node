// Информация об умном доме
let configuration = 'configuration';
// Вызов функции устройства
let callFunctionDevice = 'callFunctionDevice';
// Получения данных устройств умного дома
let getDataSmartHome = 'getDataSmarthome';
// Отправка нового состояния устройства
let deviceInfo = 'deviceInfo';
let devicesInfo = 'devicesInfo';
// Информация о подсистемах
let subsystemsInfo = 'subsystemsInfo';
// Получение устройств
let getDevices = 'getDevices';

module.exports.callFunctionDevice = callFunctionDevice;
module.exports.getDataSmartHome = getDataSmartHome;
module.exports.configuration = configuration;
module.exports.deviceInfo = deviceInfo;
module.exports.devicesInfo = devicesInfo;
module.exports.subsystemsInfo = subsystemsInfo;
module.exports.getDevices = getDevices;